# PINS-Sensor

## Description:
This repository contains the python3 code for the sensor data generation and evaluation.

## Installation:
### Shared:
```
sudo apt install python3-pip python3-dev python3-rpi.gpio git
sudo pip3 install get-mac
```

### Sensor data generation:
```
# Sunfounder Humiture Sensor (https://www.sunfounder.com/learn/sensor-kit-v2-0-for-raspberry-pi-b-plus/lesson-28-humiture-sensor-sensor-kit-v2-0-for-b-plus.html):
git clone https://github.com/sunfounder/Adafruit_Python_DHT.git
cd Adafruit_Python_DHT/
python setup.py install

# Sunfounder DS18B20 Temperature Sensor (https://www.sunfounder.com/learn/sensor-kit-v2-0-for-raspberry-pi-b-plus/lesson-26-ds18b20-temperature-sensor-sensor-kit-v2-0-for-b-plus.html):
echo 'dtoverlay=w1-gpio' >> /boot/config.txt
sudo modprobe w1-gpio
sudo modprobe w1-therm
```

### Sensor data evaluation:
```
sudo pip3 install matplotlib
```

## Hardware:
To be able to collect sensor data the following sensors are required:
* SunFounder [DS18B20](https://www.sunfounder.com/learn/sensor-kit-v2-0-for-raspberry-pi-b-plus/lesson-26-ds18b20-temperature-sensor-sensor-kit-v2-0-for-b-plus.html) Temperature Sensor
* SunFounder [Humiture](https://www.sunfounder.com/learn/lesson-23-humiture-sensor-rfid-kit-v1-0-for-arduino.html) Sensor

Once you have all sensors connect them to the Raspberry Pi like shown bellow:
![sensor setup](setup.png)