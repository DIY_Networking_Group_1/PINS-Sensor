#!/usr/bin/env python
#----------------------------------------------------------------
#	Note:
#		ds18b20's data pin must be connected to pin7.
#		replace the 28-XXXXXXXXX as yours.
#		MQTT Client tutorial:
# 		http://www.steves-internet-guide.com/into-mqtt-python-client/
#----------------------------------------------------------------
import os
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO
import time
from getmac import get_mac_address

# Humidity:
DHTPIN = 17
GPIO.setmode(GPIO.BCM)
MAX_UNCHANGE_COUNT = 100
STATE_INIT_PULL_DOWN = 1
STATE_INIT_PULL_UP = 2
STATE_DATA_FIRST_PULL_DOWN = 3
STATE_DATA_PULL_UP = 4
STATE_DATA_PULL_DOWN = 5

# Thermal:
ds18b20 = '28-0415a2686aff'
mqttClient = None
mqttClientId = str(get_mac_address(interface="eth0"))

def onMqttConnected(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

def setupMqtt():
    global mqttClient
    global mqttClientId
    mqttClient =  mqtt.Client(mqttClientId)
    mqttClient.on_connect = onMqttConnected
    mqttClient.connect("localhost", 1883, 60) 

def setupThermal():
    global ds18b20
    setupMqtt()
    for i in os.listdir('/sys/bus/w1/devices'):
        if i != 'w1_bus_master1':
            ds18b20 = i   

def setupHumidity():
    pass

def setup():
    setupThermal()
    setupHumidity()

def readThermal():
#	global ds18b20
    location = '/sys/bus/w1/devices/' + ds18b20 + '/w1_slave'
    tfile = open(location)
    text = tfile.read()
    tfile.close()
    secondline = text.split("\n")[1]
    temperaturedata = secondline.split(" ")[9]
    temperature = float(temperaturedata[2:])
    temperature = temperature / 1000
    return temperature
    
def read_dht11_dat():
    GPIO.setup(DHTPIN, GPIO.OUT)
    GPIO.output(DHTPIN, GPIO.HIGH)
    time.sleep(0.05)
    GPIO.output(DHTPIN, GPIO.LOW)
    time.sleep(0.02)
    GPIO.setup(DHTPIN, GPIO.IN, GPIO.PUD_UP)

    unchanged_count = 0
    last = -1
    data = []
    while True:
        current = GPIO.input(DHTPIN)
        data.append(current)
        if last != current:
            unchanged_count = 0
            last = current
        else:
            unchanged_count += 1
            if unchanged_count > MAX_UNCHANGE_COUNT:
                break

    state = STATE_INIT_PULL_DOWN

    lengths = []
    current_length = 0

    for current in data:
        current_length += 1

        if state == STATE_INIT_PULL_DOWN:
            if current == GPIO.LOW:
                state = STATE_INIT_PULL_UP
            else:
                continue
        if state == STATE_INIT_PULL_UP:
            if current == GPIO.HIGH:
                state = STATE_DATA_FIRST_PULL_DOWN
            else:
                continue
        if state == STATE_DATA_FIRST_PULL_DOWN:
            if current == GPIO.LOW:
                state = STATE_DATA_PULL_UP
            else:
                continue
        if state == STATE_DATA_PULL_UP:
            if current == GPIO.HIGH:
                current_length = 0
                state = STATE_DATA_PULL_DOWN
            else:
                continue
        if state == STATE_DATA_PULL_DOWN:
            if current == GPIO.LOW:
                lengths.append(current_length)
                state = STATE_DATA_PULL_UP
            else:
                continue
    if len(lengths) != 40:
        print ("Data not good, skip")
        return False

    shortest_pull_up = min(lengths)
    longest_pull_up = max(lengths)
    halfway = (longest_pull_up + shortest_pull_up) / 2
    bits = []
    the_bytes = []
    byte = 0

    for length in lengths:
        bit = 0
        if length > halfway:
            bit = 1
        bits.append(bit)
    # print ("bits: %s, length: %d" % (bits, len(bits)))
    for i in range(0, len(bits)):
        byte = byte << 1
        if (bits[i]):
            byte = byte | 1
        else:
            byte = byte | 0
        if ((i + 1) % 8 == 0):
            the_bytes.append(byte)
            byte = 0
    # print (the_bytes)
    checksum = (the_bytes[0] + the_bytes[1] + the_bytes[2] + the_bytes[3]) & 0xFF
    if the_bytes[4] != checksum:
        print ("Data not good, skip")
        return False

    return the_bytes[0], the_bytes[2]

def loop():
    global mqttClient
    global mqttClientId
    print("ClientId: " + mqttClientId)
    mqttClient.loop_start()
    mqttTopicThermal = "sensors/thermal/" + mqttClientId
    mqttTopicHum = "sensors/humidity/" + mqttClientId
    while True:
        temp = readThermal()
        if temp != None:
            print ("Current temperature : %0.2f C°" % temp)
            mqttClient.publish(mqttTopicThermal, str(temp))
            time.sleep(2)
        resultHum = read_dht11_dat()
        if resultHum != None:
            humidity, temperature = resultHum
            print ("humidity: %s %%, Temperature: %s C°" % (humidity, temperature))
            # mqttClient.publish(mqttTopicHum, str(humidity))

def destroy():
    GPIO.cleanup()

if __name__ == '__main__':
    try:
        setup()
        loop()
    except KeyboardInterrupt:
        destroy()

