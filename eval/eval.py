import os
import json
from time import sleep
import datetime
import random
import matplotlib.pyplot as plt

device1Mac = "b8:27:eb:ef:be:01"
device2Mac = "b8:27:eb:7b:4f:4f"
device3Mac = "-"

device1Temps = []
device2Temps = []
device3Temps = []

device1Hums = []
device2Hums = []
device3Hums = []

def insertInList(l, v):
    if len(l) > 100:
        l.pop(0)
    l.append((datetime.datetime.now(), float(v)))
    #l.append((datetime.datetime.now(), random.uniform(0.0, 1.9)))

def onThermalData(data):
    global device1Temps
    global device2Temps
    global device3Temps
    topicParts = data["topic"].split("/")
    if topicParts[-1] == device1Mac:
        print("New temp for device1: " + data["data"])
        insertInList(device1Temps, data["data"])
    elif topicParts[-1] == device2Mac:
        print("New temp for device2: " + data["data"])
        insertInList(device2Temps, data["data"])
    elif topicParts[-1] == device3Mac:
        print("New temp for device3: " + data["data"])
        insertInList(device3Temps, data["data"])
    else:
        print("Unknown device MAC received: " + topicParts[-1])

def onHumData(data):
    global device1Hums
    global device2Hums
    global device3Hums
    topicParts = data["topic"].split("/")
    if topicParts[-1] == device1Mac:
        print("New hum for device1: " + data["data"])
        insertInList(device1Hums, data["data"])
    elif topicParts[-1] == device2Mac:
        print("New hum for device2: " + data["data"])
        insertInList(device2Hums, data["data"])
    elif topicParts[-1] == device3Mac:
        print("New hum for device3: " + data["data"])
        insertInList(device3Hums, data["data"])
    else:
        print("Unknown device MAC received: " + topicParts[-1])

def readLinesFromFile(path):
    with open(path, "r+") as f:
        # Read all lines:
        content = f.readlines()
        # Remove all lines:
        f.truncate(0)
        # Remove whitespaces and newlines at the end of the line:
        return [x.strip() for x in content]

def plot(l1, l2, num, nameX, nameY, title):
    x1 = []
    y1 = []
    for t1, t2 in l1:
        x1.append(t1)
        y1.append(t2)

    x2 = []
    y2 = []
    for t1, t2 in l2:
        x2.append(t1)
        y2.append(t2)

    fig = plt.figure(num)
    fig.canvas.set_window_title(title)
    plt.xlabel(nameX)
    plt.ylabel(nameY)
    ax = fig.add_subplot(111)
    ax.grid(True, which='both')
    ax.plot(x1, y1, 'ro', label='Device 1')
    ax.plot(x1, y1, 'k')
    ax.plot(x2, y2, 'bo', label='Device 2')
    ax.plot(x2, y2, 'k')

def getTime():
    return datetime.datetime.now().strftime("%H:%M:%S")

def savePlot():
    global device1Hums
    global device2Hums
    global device1Temps
    global device2Temps

    plot(device1Temps, device2Temps, 1, 'Time', 'Temp in °C', 'Time/Temp - ' + getTime())
    plot(device1Hums, device2Hums, 2, 'Time', 'Hum. in %', 'Time/Hum - '+ getTime())
    # plt.show()
    plt.pause(0.05)

def evalLines(lines):
    for l in lines:
        try:
            data = json.loads(l)
            if (("topic" not in data) or ("data" not in data)):
                print("Line does not contain topic and data: " + str(data))
                continue

            if "thermal" in data["topic"]:
                onThermalData(data)
            elif "humidity" in data["topic"]:
                onHumData(data)
            else:
                print("Not hum/thermal line")
                continue
        except ValueError:
            print("Failed to parse line: " + l)

if __name__=="__main__":
    path = "/tmp/PINS_BM-out.txt"
    # path = "/tmp/2"
    while 1:
        if os.path.exists(path):
            lines = readLinesFromFile(path)
            print('Read: ' + str(len(lines)) + ' lines')
            evalLines(lines)
            savePlot()
        sleep(10)
